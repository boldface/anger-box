<?php
/**
 * Anger Box
 *
 * Bootstrap to load the theme.
 *
 * @package Anger Box
 * @author  Nathan Johnson
 * @license GPL-2.0+
 * @link    http://www.boldfacedesign.com/
 */

namespace Boldface\AngerBox;

//* Class autoloader
require( __DIR__ . '/lib/autoload.php' );
\spl_autoload_register( __NAMESPACE__ . '\\autoload::load' );

//* Add action to the genesis_pre hook to setup the theme
\add_action( 'genesis_pre', [ new setup(), 'register' ] );
