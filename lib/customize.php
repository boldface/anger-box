<?php
/**
 * Anger Box
 *
 * This file adds the Customizer additions to the Anger Box Theme.
 *
 * @package Anger Box
 * @author  Nathan Johnson
 * @license GPL-2.0+
 * @link    http://www.boldfacedesign.com/
 */

namespace Boldface\AngerBox;

class customize {

	/**
	 * Constructor
	 *
	 * @access public
	 * @since 1.0
	 */
	public function __construct() {

	}

	/**
	 * Get default link color for Customizer.
	 *
	 * @access public
	 * @since 1.0.0
	 *
	 * @return string Hex color code for link color.
	 */
	public static function get_default_link_color() {
		return '#c3251d';
	}

	/**
	* Get default accent color for Customizer.
	*
	* @access public
	* @since 1.0.0
	*
	* @return string Hex color code for accent color.
	*/
	public static function get_default_accent_color() {
		return '#c3251d';
	}

	/**
	* Register settings and controls with the Customizer.
	*
	* @access public
	* @since 1.0.0
	*
	* @param WP_Customize_Manager $wp_customize Customizer object.
	*/
	public function register() {

		global $wp_customize;

		$wp_customize->add_setting( 'anger_box_link_color', [
			'default'           => self::get_default_link_color(),
			'sanitize_callback' => 'sanitize_hex_color',
		] );


		$wp_customize->add_control(
			new \WP_Customize_Color_Control(
				$wp_customize,
				'anger_box_link_color',
				[
					'description' => __( 'Change the default color for linked titles, menu links, post info links and more.', 'anger-box' ),
					'label'       => __( 'Link Color', 'anger-box' ),
					'section'     => 'colors',
					'settings'    => 'anger_box_link_color',
				]
			)
		);

		$wp_customize->add_setting( 'anger_box_accent_color', [
			'default'           => self::get_default_accent_color(),
			'sanitize_callback' => 'sanitize_hex_color',
		] );

		$wp_customize->add_control(
			new \WP_Customize_Color_Control(
				$wp_customize,
				'anger_box_accent_color',
				[
					'description' => __( 'Change the default color for button hovers.', 'anger-box' ),
					'label'       => __( 'Accent Color', 'anger-box' ),
					'section'     => 'colors',
					'settings'    => 'anger_box_accent_color',
				]
			)
		);
	}
}
