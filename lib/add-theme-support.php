<?php
/**
 * Anger Box
 *
 *
 *
 * @package Anger Box
 * @author  Nathan Johnson
 * @license GPL-2.0+
 * @link    http://www.boldfacedesign.com/
 */

namespace Boldface\AngerBox;

class add_theme_support {

  private $supports = [
    //* Add HTML5 markup structure
    'html5' => [
      'caption',
      'comment-form',
      'comment-list',
      'gallery',
      'search-form',
    ],
    //* Add Accessibility support
    'genesis-accessibility' => [
      '404-page',
      'drop-down-menu',
      'headings',
      'rems',
      'search-form',
      'skip-links',
    ],
    //* Add viewport meta tag for mobile browsers
    'genesis-responsive-viewport',
    //* Add support for custom header
    'custom-header' =>  [
    	'width'           => 600,
    	'height'          => 160,
    	'header-selector' => '.site-title a',
    	'header-text'     => false,
    	'flex-height'     => true,
    ],
    //* Add support for custom background
    'custom-background',
    //* Add support for after entry widget
    'genesis-after-entry-widget-area',
    //* Add support for 3-column footer widgets
    'genesis-footer-widgets' => 0,
    //* Rename primary navigation menus
    'genesis-menus' => [
      'primary' => 'Header Menu',
    ],
    'custom-logo' => [
      'height'      => 100,
      'width'       => 400,
      'flex-height' => true,
      'flex-width'  => true,
    ],
  ];

  public function __construct() {
  }

  public function register() {

    foreach( $this->supports as $key => $args ) {
      if( is_numeric( $key ) ) {
        $key = $args;
        $args = null;
      }
      $args = apply_filters( "anger_box_supports_$key", $args );

      \add_theme_support( $key, $args );
    }
  }
}
