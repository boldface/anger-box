<?php
/**
 * Anger Box
 *
 *
 *
 * @package Anger Box
 * @author  Nathan Johnson
 * @license GPL-2.0+
 * @link    http://www.boldfacedesign.com/
 */

namespace Boldface\AngerBox;

defined( 'ABSPATH' ) or die();

class autoload {

  private static function from_this_namespace( $class ) {

    //* $class needs to be a string
    if( ! is_string( $class ) ) {
      return;
    }

    //* Short circuit if class isn't from this namespace
    if( __NAMESPACE__ != substr( $class, 0, strlen( __NAMESPACE__ ) ) ) {
      return false;
    }
    return true;
  }

  public static function class_exists( $class ) {

    //* $class needs to be a string
    if( ! is_string( $class ) ) {
      return;
    }

    if( '' === self::file_name( $class ) ) {
      return false;
    }
    return true;
  }

  public static function file_name( $class ) {

    //* $class needs to be a string
    if( ! is_string( $class ) ) {
      return;
    }
    
    //* Remove namespace
    $class = str_replace( __NAMESPACE__, '', $class );

    //* Replace backslashes with forward slashes
    $class = str_replace( '\\', '/', $class );

    //* Replace underscores with dashes
    $class = str_replace( '_', '-', $class );

    //* List of directories in which to check for the class
    $dir_list = [
      __DIR__,
      \dirname( __DIR__ ) . '/class',
      \dirname( __DIR__ ) . '/src',
    ];

    foreach( $dir_list as $dir ) {
      $file = $dir . $class . '.php';

      if( file_exists( $file ) ) {
        return $file;
      }
    }
    return '';
  }

  public static function load( $class ) {

    //* $class needs to be a string
    if( ! is_string( $class ) ) {
      return;
    }

    //* Short circuit if class isn't from this namespace
    if( ! self::from_this_namespace( $class ) ) {
      return;
    }

    $file = self::file_name( $class );

    if( '' !== $file ) {
      require $file;
      return;
    }
  }
}
