<?php
/**
 * Anger Box entry
 *
 * @package Anger Box
 * @author  Nathan Johnson
 * @license GPL-2.0+
 * @link    http://www.boldfacedesign.com/
 */

namespace Boldface\AngerBox;

class entry {

  /**
   * Constructor
   *
   * @access public
   * @since 1.0.0
   */
  public function __construct() {
  }

  /**
   * Add actions and filters to load the entry
   *
   * @access private
   * @since 1.0.0
   *
   * @uses add_action()
   */
  public function register() {
    \remove_action( 'genesis_after_entry', 'genesis_after_entry_widget_area' );
    \add_action( 'genesis_after_entry', [ $this, 'after_entry_widget_area' ] );

    //* Only use one layout
    $this->layouts( [ 'full-width-content' ] );

    //* Only use one sidebar
    $this->sidebars( [ 'after-entry' ] );
  }

  public function after_entry_widget_area() {
    //* Escape early if the page isn't a singular page or post
    if( ! is_singular( [ 'post', 'page' ] ) ) {
      return;
    }
    //* Or if it's the contact page
    if( \is_page() && 'contact' === \get_queried_object()->post_name ) {
      return;
    }

    //* Otherwise, add an after entry widget
    \genesis_widget_area( 'after-entry', [
      'before' => '<div class="after-entry-widget-area"><article class="entry">',
      'after'  => '</article></div>',
    ] );
  }

  public function layouts( array $layouts = [] ) {

    if( count( $layouts ) < 1 ) {
      return;
    }

    $genesis_layouts = \genesis_get_layouts();

    foreach( $genesis_layouts as $layout => $value ) {
      if( is_string( $layout ) && ! in_array( $layout, $layouts ) ) {
        \genesis_unregister_layout( $layout );
      }
    }

    //* If there's only one layout, set it as the default
    if( 1 === count( $layouts ) ) {
      \genesis_set_default_layout( $layouts[0] );
    }
  }

  public function sidebars( array $sidebars = [] ) {

    if( count( $sidebars ) < 1 ) {
      return;
    }

    //* Default Genesis sidebars
    $genesis_sidebars = [
      'sidebar',
      'sidebar-alt',
      'header-right',
      'after-entry'
    ];

    foreach( $genesis_sidebars as $sidebar ) {
      if( is_string( $sidebar ) && ! in_array( $sidebar, $sidebars ) ) {
        \unregister_sidebar( $sidebar );
      }
    }
  }
}
