<?php
/**
 * Anger Box
 *
 * @package Anger Box
 * @author  Nathan Johnson
 * @license GPL-2.0+
 * @link    http://www.boldfacedesign.com/
 */

namespace Boldface\AngerBox;

class setup {

  /**
   * @var $genesis_lib The Genesis Framework library directory
   *
   * @access private
   * @since 1.0.0
   */
  private $genesis_lib;

  /**
   * @var $child_lib The child theme library directory
   *
   * @access private
   * @since 1.0.0
   */
  private $child_lib;

  /**
   * Constructor
   *
   * @access public
   * @since 1.0.0
   *
   * @uses get_template_directory()
   * @uses get_stylesheet_directory()
   */
  public function __construct() {
    $this->genesis_lib = \get_template_directory() . '/lib';
    $this->child_lib = \get_stylesheet_directory() . '/lib';
    
    $this->define_theme_constants();
  }

  /**
   * The register method should be called from the earliest hook possible
   * Add actions and filters to load the theme
   *
   * @access private
   * @since 1.0.0
   *
   * @uses add_action()
   */
  public function register() {

    //* Start the Genesis engine
    \add_action( 'genesis_init', [ $this, 'genesis' ], 15 );

    //* Load the child theme text domain
    \add_action( 'after_setup_theme', [ $this, 'text_domain' ], 15, 1 );

    //* Enqueue scripts and styles
    \add_action( 'wp_enqueue_scripts', [ $this, 'enqueue_scripts' ] );

    //* Add image size
    \add_action( 'init', [ $this, 'add_image_size' ] );

    \add_action( 'genesis_init', [ new add_theme_support(), 'register' ], 15 );
    \add_action( 'customize_register', [ new customize(), 'register' ] );
    \add_action( 'wp_enqueue_scripts', [ new output(), 'register' ] );
    \add_action( 'genesis_init', [ new theme_defaults(), 'register' ] );
    \add_action( 'init', [ new background() , 'register' ] );

    if( ! is_admin() ) {
      \add_action( 'init', [ new header() , 'register' ] );
      \add_action( 'init', [ new entry(), 'register' ] );
      \add_action( 'init', [ new footer(), 'register' ] );
    }

    \add_filter( 'upload_mimes', [ $this, 'add_mime_types' ] );

  }

  /**
   * Define the theme constants from style.css
   *
   * @access private
   * @since 1.0.0
   *
   * @uses wp_get_theme()
   */
  private function define_theme_constants() {

    $child_theme = \wp_get_theme();

    //* Define child theme constants
    define( 'CHILD_THEME_NAME', $child_theme->get( 'Name' ) );
    define( 'CHILD_THEME_URL', $child_theme->get( 'ThemeURI' ) );
    define( 'CHILD_THEME_VERSION', $child_theme->get( 'Version' ) );
    define( 'CHILD_THEME_TEXT_DOMAIN', $child_theme->get( 'TextDomain' ) );
  }

  /**
   * Start the Genesis engine
   *
   * @access public
   * @since 1.0.0
   */
  public function genesis() {
    require_once( $this->genesis_lib . '/init.php' );
  }

  /**
   * Load text domain
   *
   * @access private
   * @since 1.0.0
   *
   * @uses load_child_theme_textdomain()
   * @uses apply_filters()
   * @uses get_stylesheet_directory()
   */
  public function text_domain() {
    \load_child_theme_textdomain( 'anger-box', apply_filters( 'child_theme_textdomain', get_stylesheet_directory() . '/languages', 'anger-box' ) );
  }

  /**
   * Enqueue theme scripts and styles
   *
   * @access public
   * @since 1.0.0
   *
   * @uses wp_enqueue_style()
   * @uses wp_enqueue_script()
   * @uses get_stylesheet_directory_uri()
   * @uses wp_localize_script()
   */
  public function enqueue_scripts() {
    \wp_enqueue_style( 'anger-box-fonts', '//fonts.googleapis.com/css?family=Source+Sans+Pro:400,600,700', array(), 'anger-box' );
    \wp_enqueue_style( 'dashicons' );

    \wp_enqueue_script( 'genesis-sample-responsive-menu', get_stylesheet_directory_uri() . '/js/responsive-menu.js', array( 'jquery' ), '1.0.0', true );
    $output = array(
      'mainMenu' => __( 'Menu', 'anger-box' ),
      'subMenu'  => __( 'Menu', 'anger-box' ),
    );
    \wp_localize_script( 'genesis-sample-responsive-menu', 'genesisSampleL10n', $output );
  }

  /**
   * Add image size
   *
   * @access public
   * @since 1.0.0
   *
   * @uses add_image_size()
   */
  public function add_image_size() {
    \add_image_size( 'featured-image', 720, 400, TRUE );
  }

  public function add_mime_types( $mimes ) {
    if( ! \current_user_can( 'update_core' ) ) {
      return $mimes;
    }
    $mimes['svg'] = 'image/svg+xml';
    $mimes['svgz'] = 'image/svg+xml';

    return $mimes;
  }
}
