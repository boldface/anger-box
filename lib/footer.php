<?php
/**
 * Anger Box footer
 *
 * @package Anger Box
 * @author  Nathan Johnson
 * @license GPL-2.0+
 * @link    http://www.boldfacedesign.com/
 */

namespace Boldface\AngerBox;

class footer {

  /**
   * Constructor
   *
   * @access public
   * @since 1.0.0
   */
  public function __construct() {
  }

  /**
   * Add actions and filters to load the footer
   *
   * @access private
   * @since 1.0.0
   *
   * @uses add_action()
   */
  public function register() {
    \add_filter( 'genesis_footer_creds_text', [ $this, 'copyright_text' ] );
  }

  public function copyright_text() {
    $first_year = '2017';
    $current_year = ( string ) date( 'Y' );

    if( $first_year >= $current_year ) {
      $years = $current_year;
    }
    else {
      $years = $first_year . '-' . $current_year;
    }

    return "Copyright &copy; $years Leah Collery";
  }
}
