<?php
/**
 * Anger Box header
 *
 * @package Anger Box
 * @author  Nathan Johnson
 * @license GPL-2.0+
 * @link    http://www.boldfacedesign.com/
 */

namespace Boldface\AngerBox;

class header {

  /**
   * Constructor
   *
   * @access public
   * @since 1.0.0
   */
  public function __construct() {
  }

  /**
   * Add actions and filters to load the header
   *
   * @access private
   * @since 1.0.0
   *
   * @uses add_action()
   */
  public function register() {
    //* Remove most of the header
    \remove_action( 'genesis_header', 'genesis_do_header' );

    //* Add the Primary Navigation menu to the header
    \remove_action( 'genesis_after_header', 'genesis_do_nav' );
    \add_action( 'genesis_header', 'genesis_do_nav', 10 );

    //* Add the logo to display on mobile
    \add_action( 'genesis_header', [ $this, 'the_logo' ], 11 );

    //* Replace home link with logo
    \add_filter( 'wp_nav_menu_items', [ $this, 'replace_home_link_with_logo' ], 10, 1 );
  }

  public function the_logo() {
    ob_start(); ?>
    <div class="mobile">
    <?php \the_custom_logo(); ?>
    </div> <?php
    echo ob_get_clean();
  }

  public function replace_home_link_with_logo( $items ) {
    if( ! function_exists( 'get_custom_logo' ) || '' === \get_custom_logo() ) {
      return $items;
    }

    $i = explode( '<li', $items );

    $new = '';
    foreach( $i as $item ) {

      if( '' === $item ){
        continue;
      }
      $item = '<li' . $item;

      if(
        stripos( $item, 'href="' . \home_url() . '"' )  ||
        stripos( $item, 'href="' . \home_url() . '/"' ) ||
        stripos( $item, 'href="/"' )
      ) {
        $class_start = stripos( $item, 'class="' );
        $class_end = stripos( $item, '"', $class_start + 7 );
        $class_length = $class_end - $class_start;
        $class = substr( $item, $class_start, $class_length );

        $item = str_replace( $class, $class . ' menu-item-home', $item );

        $a_start = stripos( $item, '<a ' );
        $a_end = stripos( $item, '</a>', $a_start );
        $a_length = $a_end - $a_start;
        $a = substr( $item, $a_start, $a_length );
        $item = str_replace( $a, \get_custom_logo(), $item );
      }
      $new .= $item;
    }
    return $new;
  }
}
