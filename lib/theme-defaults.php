<?php
/**
 * This file adds the default theme settings to the Anger Box Theme.
 *
 * @package Anger Box
 * @author  Nathan Johnson
 * @license GPL-2.0+
 * @link    http://www.boldfacedesign.com/
 */

namespace Boldface\AngerBox;


class theme_defaults {

	public function __construct() {

	}

	/**
	 *
	 */
	public function register() {
		\add_filter( 'genesis_theme_settings_defaults', [ $this, 'defaults' ] );
		\add_action( 'after_switch_theme', [ $this, 'setting_defaults' ] );
	}

	/**
	* Updates theme settings on reset.
	*
	* @since 1.0.0
	*/
	function defaults( array $defaults = [] ) {

		$defaults['blog_cat_num']              = 6;
		$defaults['content_archive']           = 'full';
		$defaults['content_archive_limit']     = 0;
		$defaults['content_archive_thumbnail'] = 0;
		$defaults['posts_nav']                 = 'numeric';
		$defaults['site_layout']               = 'content-sidebar';

		return $defaults;

	}

	/**
	* Updates theme settings on activation.
	*
	* @since 2.2.3
	*/
	function setting_defaults() {
		if ( function_exists( 'genesis_update_settings' ) ) {
			\genesis_update_settings( $this->defaults() );
		}
		\update_option( 'posts_per_page', 6 );
	}
}
