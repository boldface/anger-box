<?php
/**
 * Custom background
 *
 * @package Anger Box
 * @author  Nathan Johnson
 * @license GPL-2.0+
 * @link    http://www.boldfacedesign.com/
 */

namespace Boldface\AngerBox;

class background {

  /**
   * @var $background_image The custom background image
   *
   * @access private
   * @since 1.0.0
   */
  private $background_image;

    /**
     * @var $background_image_id The custom background image ID
     *
     * @access private
     * @since 1.0.0
     */
  private $background_image_id;

  /**
   * @var $background_element The custom background element
   *
   * @access private
   * @since 1.0.0
   */
  private $background_element;

  /**
   * Constructor
   *
   * @access public
   * @since 1.0.0
   *
   * @uses get_background_image()
   */
  public function __construct() {
    $this->background_image = \get_background_image();
  }

  /**
   * Register the background class
   *
   * @access public
   * @since 1.0.0
   *
   * @uses custom_header_background()
   */
  public function register() {

    \add_action( 'customize_save_after', [ $this, 'reset_transients' ] );

    if( $this->background_image ) {

      //* Add background-small image size
      \add_image_size( 'background-small', 420, 262, TRUE );

      //* Get the background image ID
      $this->background_image_id = $this->image_id_from_url( $this->background_image );

      //* Do the custom header background
      $this->custom_header_background();

    }
  }

  /**
   * Reset the transients
   *
   * @access private
   * @since 1.0.0
   *
   * @uses delete_transient()
   */
  public function reset_transients() {
    \delete_transient( 'boldface_responsive_background_css' );
  }

  /**
   * Attempt to get the image ID from the image URL
   *
   * @param string $url The full image URL as a string
   *
   * @access private
   * @since 1.0.0
   *
   * @uses custom_header_background()
   *
   * @return int The image ID or -1 for error
   */
private function image_id_from_url( $url ) {

    //* URL needs to be a string
    if( ! is_string ( $url ) ) {
      return;
    }

    //* Make sure we're dealing with a file uploaded to the upload directory
    $upload_dir_paths = \wp_upload_dir();
    if( false === strpos( $url, $upload_dir_paths[ 'baseurl' ] ) ) {
      return;
    }

    //* Remove auto-generaged image sizes if present
    $url = preg_replace( '/-\d+x\d+(?=\.(jpg|jpeg|png|gif)$)/i', '', $url );

    //* Remove the image upload base URL
    $url = str_replace( $upload_dir_paths[ 'baseurl' ] . '/', '', $url );

    //* We'll be accessing the WP database directly
    global $wpdb;

    //* Prepare and get our SQL query
    $id = $wpdb->get_var( $wpdb->prepare(
      "SELECT wposts.ID FROM $wpdb->posts wposts, $wpdb->postmeta wpostmeta WHERE wposts.ID = wpostmeta.post_id AND wpostmeta.meta_key = '_wp_attached_file' AND wpostmeta.meta_value = '%s' AND wposts.post_type = 'attachment'",
      $url
    ) );

    if( is_int( (int) $id ) ) {
      return (int) $id;
    }
    return -1;
  }

  /**
   * Remove the default custom header background action and add a new one
   *
   * @access private
   * @since 1.0.0
   *
   * @uses add_action()
   * @uses remove_action()
   */
  private function custom_header_background() {
    \remove_action( 'wp_loaded', '_custom_header_background_just_in_time' );
    \add_action( 'wp_loaded', [ $this, 'custom_header_background_just_in_time' ] );
  }

  /**
   * Add actions to several hooks
   *
   * @access public
   * @since 1.0.0
   *
   * @uses add_action()
   */
  public function custom_header_background_just_in_time() {
    \add_action( 'wp_footer', [ $this, 'custom_header_background_html' ] );
    \add_action( 'wp_enqueue_scripts', [ $this , 'custom_header_background_css' ] );
    \add_action( 'wp_footer', [$this, 'custom_header_background_js' ] );
  }

  /**
   * Add inline CSS for the background image
   *
   * @access public
   * @since 1.0.0
   *
   * @uses wp_add_inline_style()
   */
  public function custom_header_background_css() {
    $css =  $this->responsive_background( $this->background_image_id );
    \wp_add_inline_style( 'anger-box', $css );
  }

  /**
   * Get the resonsive background CSS
   *
   * @param int $id ID of the background image
   *
   * @access private
   * @since 1.0.0
   *
   * @return string The resonsive background CSS
   */
  private function responsive_background( $id ) {

    //* Make sure $id is an integer
    if( ! is_integer( $id ) ) {
      return;
    }

    $transient = 'boldface_responsive_background_css';

    //* If a transient exists, let's use that
    if( false !== ( $css = \get_transient( $transient ) ) ) {
      return \apply_filters( $transient, $css );
    }

    //* Otherwise, determine our CSS
    $css = sprintf( '#background{background-image:URL(%1$s);}%2$s',
      $this->default_background_image( $id ),
      $this->media_queries( $id )
    );

    $css = str_replace( [ ' ', PHP_EOL ], '', $css );

    //* Set a transient so we don't have to compute this on every page load
    \set_transient( $transient, $css, 1440 );

    //* And return it
    return \apply_filters( $transient, $css );
  }

  /**
   * Get the URL of the default background image
   *
   * @param int $id ID of the background image
   *
   * @access private
   * @since 1.0.0
   *
   * @return string The default background image URL
   */
  private function default_background_image( $id ) {

    //* Make sure $id is an integer
    if( ! is_integer( $id ) ) {
      return;
    }

    //* Get the sizes of the image from the ID
    $sizes = $this->sorted_sizes( $id );

    //* Return the smallest image size
    return $this->background_image_sized(
      $this->background_image,
      array_pop( $sizes )
    );
  }

  /**
   * Sort an array in descending order based on the first index value
   *
   * @param array $sizes Array of image sizes
   *
   * @access private
   * @since 1.0.0
   *
   * @return array An associative array of sizes sorted in descending order
   */
  private function sort_sizes( $sizes ) {
    //* This is equivalent to returning the spaceship operator $b[0] <=> $a[0]
    usort( $sizes, function( $a, $b ){
      if( $b[0] > $a[0] ) {
        return 1;
      }
      elseif( $b[0] == $a[0] ) {
        return 0;
      }
      else{
        return -1;
      }
    } );
    return $sizes;
  }

  /**
   * Transform the default image to a sized image
   *
   * @param string $image_url The URL of the image
   * @param array  $size      The 2D array of sizes
   *
   * @access private
   * @since 1.0.0
   *
   * @return string The new image URL
   */
  private function background_image_sized( $image_url, array $size ) {

    //* Make sure $id is a string
    if( ! is_string( $image_url ) ) {
      return;
    }

    //* Replace background image with corresponding size
    return preg_replace(
      '/(\.(jpg|jpeg|png|gif)$)/i',
      "-$size[0]x$size[1]" . '\1',
      $image_url
    );
  }

  /**
   * Given an image ID, return the CSS media queries
   *
   * @param int $id The ID of the image
   *
   * @access private
   * @since 1.0.0
   *
   * @return string CSS media queries
   */
  private function media_queries( $id ) {

    //* Make sure $id is an integer
    if( ! is_integer( $id ) ) {
      return;
    }

    //* Get the sorted sizes of the image from the ID
    $sizes = $this->sorted_sizes( $id );

    //* Begin inline CSS @media queries
    $css = '';

    //* Loop through the sizes
    for( $i = 0; $i < count( $sizes ); $i++ ) {

      //* Calculate default and large sizes
      $size = $sizes[$i];
      $large_size = $i > 0 ? $sizes[$i-1] : $sizes[$i];

      //* Add default image for large screens
      if( 0 === $i ) {
        $css .= sprintf( '
        @media (min-width: %1$spx) {
          #background {
            background-image: URL( %2$s );
          }
        }',
          $size[0]+1,
          $this->background_image );
      }

      //* Add new CSS media queries
      $css .= sprintf( '
        @media (max-width: %1$spx) {
          #background {
            background-image: URL( %2$s );
          }
        }
        @media (max-width: %1$spx) and ((-webkit-min-device-pixel-ratio: 2),(min-resolution:192dpi)) {
          #background {
            background-image: URL( %3$s );
          }
        }
        @media (max-width: %1$spx) {
          #background {
            background-image: -webkit-image-set(
              URL( %2$s ) 1x,
              URL( %3$s ) 2x
            );
          }
          #background {
            background-image: image-set(
              URL( %2$s ) 1x,
              URL( %3$s ) 2x
            );
          }
        }',
        $size[0],
        $this->background_image_sized( $this->background_image, $size ),
        $this->background_image_sized( $this->background_image, $large_size )
      );
    }
    return $css;
  }

  /**
   * Get an array of sizes of image ID
   *
   * @param int $id Image ID
   *
   * @access private
   * @since 1.0.0
   *
   * @uses wp_get_attachment_image_srcset()
   *
   * @return array An associative array of sizes
   */
  private function sizes( $id ) {

    //* Make sure $id is an integer
    if( ! is_integer( $id ) ) {
      return;
    }

    $sizes = \wp_get_attachment_image_srcset( $id );
    $sizes = explode( ',', $sizes );
    $s =[];
    foreach( $sizes as $size ) {
      preg_match( '/\d+x\d+/i', $size, $matches );
      $s[] = explode( 'x', $matches[0] );
    }
    return $s;
  }

  /**
   * Wrapper of sizes() that additionally sorts the sizes
   *
   * @param int $id Image ID
   *
   * @access private
   * @since 1.0.0
   *
   * @return array An associative array of sizes
   */
  private function sorted_sizes( $id ) {

    //* Make sure $id is an integer
    if( ! is_integer( $id ) ) {
      return;
    }
    
    return $this->sort_sizes( $this->sizes( $id ) );
  }

  /**
   * Enqueue JS for the background image
   *
   * @access public
   * @since 1.0.0
   *
   * @uses wp_enqueue_script()
   */
  public function custom_header_background_js() {
    \wp_enqueue_script( 'grayscale-background', \get_stylesheet_directory_uri() . '/js/grayscale-background.js' );
  }

  /**
   * Echo blank div
   *
   * @access public
   * @since 1.0.0
   */
  public function custom_header_background_html() {
    $args = $this->parse_background_element();

    echo sprintf(
      '<%1$s id="%2$s" class="%3$s"></%1$s>',
      $args[ 'element' ],
      $args[ 'id' ],
      $args[ 'class' ]
    );
  }

  private function parse_background_element() {

    $this->background_element = '#background';

    //* Default arguments
    $args = [
      'element' => 'body',
      'id'      => '',
      'class'   => ''
    ];

    if( ! isset( $this->background_element ) ) {
      return $args;
    }

    $first_char = substr( $this->background_element, 0, 1 );

    if( '#' === $first_char ) {
      $args[ 'element' ] = 'div';
      $args[ 'id' ] = substr( $this->background_element, 1 );
    }
    elseif( '.' === $first_char ) {
      $args[ 'element' ] = 'div';
      $args[ 'class' ] = substr( $this->background_element, 1 );
    }
    else {
      $args[ 'element' ] = $this->background_element;
    }
    return $args;
  }
}
