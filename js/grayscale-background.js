/*
  Many thanks to Phil Ricketts for the elegant solution to find the vertical scroll percentage
  https://stackoverflow.com/questions/2387136/cross-browser-method-to-determine-vertical-scroll-percentage-in-javascript#8028584
*/

document.addEventListener('scroll', function() {

  var h = document.documentElement,
    b = document.body,
    st = 'scrollTop',
    sh = 'scrollHeight';

  /*  var percent = ((h[st] || b[st]) / ((h[sh] || b[sh]) - h.clientHeight) ); */
  var percent =     ((h[st] || b[st]) / ((h[sh] || b[sh]) - h.clientHeight) );
  var inverse = 1 - ((h[st] || b[st]) / ((h[sh] || b[sh]) - h.clientHeight) );

  var f = parseInt( 255 * percent );

  var element = document.getElementById('background');

  element.style.webkitFilter = 'grayscale( ' + inverse + ' )';
  element.style.filter = 'grayscale( ' + inverse + ' )';


  var elements = document.getElementsByClassName( 'entry' );

  for( var i = 0; i < elements.length; i++ ) {
/*    elements[i].style.backgroundColor = '#' + f + f; */
  }

});
